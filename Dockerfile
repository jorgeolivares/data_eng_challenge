FROM python:3.7-alpine

WORKDIR /home/data_eng_challenge

COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn
RUN venv/bin/pip install -U flask

COPY app app
COPY data_eng_challenge.py boot.sh ./
RUN chmod +x boot.sh

ENV FLASK_APP data_eng_challenge.py

RUN chown -R root:root ./

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]