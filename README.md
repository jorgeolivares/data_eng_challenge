# Basic API: Flask, SQLite and Docker

A basic project to deploy locally a basic API written in Flask with an inmemory sqlite database.

The API can write names to the DB as well as get all the names stored in it.

## Project Structure

The project structure is the following:

```console
data_eng_challenge/
├── app
│   ├── db_utils.py
│   ├── __init__.py
│   ├── models.py
│   │   ├── db_utils.cpython-38.pyc
│   │   ├── __init__.cpython-38.pyc
│   │   ├── models.cpython-38.pyc
│   │   └── routes.cpython-38.pyc
│   └── routes.py
├── boot.sh
├── data_eng_challenge.py
├── Dockerfile
├── README.md
├── requirements.txt
└── tests
    ├── __init__.py
    └── test_app
        ├── __init__.py
        ├── test_db_utils.py
        └── test_models.py
```

## Requirements

* [Docker Engine](https://docs.docker.com/engine/install/ubuntu/)

## Installation

Once you have installed the required software, proceed with the following steps:

```
git clone https://gitlab.com/jorgeolivares/data_eng_challenge.git
cd ./data_eng_challenge
sudo docker build -f Dockerfile -t data_eng_challenge:latest .
docker run -p 5001:5000 -d data_eng_challenge:latest
```

To check that your container is running, run the following command:

```
docker ps
```

Yor should see something similar to this in your console:
```
CONTAINER ID   IMAGE                       COMMAND       CREATED         STATUS         PORTS                    NAMES
35e429efd7fe   data_eng_challenge:latest   "./boot.sh"   3 seconds ago   Up 2 seconds   0.0.0.0:5001->5000/tcp   elated_wu
```

You are ready to go! Go to your browser and type: or click the following link:

[0.0.0.0:5001](0.0.0.0:5001)

You should be able to see the welcome message:

"Welcome to the Junior Data Engineering test for LiveEO!!!"

## Usage examples

- To add a name:

[0.0.0.0:5001/names/add?name=test_name](0.0.0.0:5001/names/add?name=test_name)

- To view names stored in the DB:

[0.0.0.0:5001/names/](0.0.0.0:5001/names)

- If you try adding the same name twice, you should see the message:
The name already exists
  
- If you try adding a name longer than 20 characters, you should 
  see the message: The name must not exceed 20 characters
  
## When Finish

Run the following commands to stop the docker container:

```
docker ps
```

Copy the container id and do
  
```
docker stop <container id>
```

To remove the container, please run the command:

```
docker rm <container id>
```