from app import create_table, get_db_connection
from app.db_utils import get_cursor
from app.models import add_name_to_table, is_name_valid, get_names

TEST_NAME = 'test_name'
TEST_NAME_OTHER = 'test_name_other'
TEST_NAME_LONG = TEST_NAME*10
TEST_NAME_LIST = [
    'test_name_1',
    'test_name_2',
    'test_name_3'
]


def test_create_table():
    create_table(table_name='names')
    test_sql_create_table_query = f"SELECT * FROM sqlite_master " \
                                  f"WHERE type='table' AND name='names';"
    conn = get_db_connection()
    cur = get_cursor(conn)
    result = cur.execute(test_sql_create_table_query).fetchall()
    assert result


def test_add_name_to_table():
    create_table(table_name='names')
    add_name_to_table(name=TEST_NAME)
    test_sql_add_name_to_table = f"SELECT * FROM main.names " \
                                 f"WHERE name = '{TEST_NAME}';"

    conn = get_db_connection()
    cur = get_cursor(conn)
    result = cur.execute(test_sql_add_name_to_table).fetchone()
    assert result['name'] == TEST_NAME


def test_is_name_valid():
    create_table(table_name='names')

    assert is_name_valid(name=TEST_NAME_OTHER) is None

    add_name_to_table(name=TEST_NAME_OTHER)

    try:
        is_name_valid(TEST_NAME_OTHER)
        is_name_valid(TEST_NAME_LONG)
    except Exception as e:
        assert True


def test_get_names():
    create_table(table_name='names')

    for name in TEST_NAME_LIST:
        add_name_to_table(name)

    response = get_names()
    name_list = [item['name'] for item in response]

    assert all(item in name_list for item in TEST_NAME_LIST)
