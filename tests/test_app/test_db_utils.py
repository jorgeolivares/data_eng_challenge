from sqlite3 import Cursor

from app import get_db_connection
from app.db_utils import get_cursor


def test_get_db_connection():
    conn = get_db_connection()
    cur = get_cursor(conn)
    result = cur.execute("SELECT 1").fetchall()
    assert result


def test_get_cursor():
    conn = get_db_connection()
    cur = get_cursor(conn=conn)
    assert isinstance(cur, Cursor)
