from flask import Flask

from app.db_utils import get_db_connection
from app.models import create_table

app = Flask(__name__)

from app import routes

conn = get_db_connection()
create_table(table_name='names')

if __name__ == "__main__":
    app.run()
