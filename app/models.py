import logging
from typing import List

from app.db_utils import get_db_connection, get_cursor


class NameAlreadyExistsError(Exception):
    code = 409
    message = 'The name already exists'


class InvalidNameLengthError(Exception):
    code = 409
    message = 'The name must not exceed 20 characters'


def create_table(table_name: str) -> None:
    sql_create_table_query = f"CREATE TABLE IF NOT EXISTS {table_name} (" \
                             f"id INTEGER PRIMARY KEY AUTOINCREMENT, " \
                             f"name VARCHAR(20) NOT NULL UNIQUE);"
    conn = get_db_connection()

    c = get_cursor(conn)
    c.execute(sql_create_table_query)
    conn.commit()
    logging.info(f"Table {table_name} created successfully")


def add_name_to_table(name: str) -> str:
    sql_add_name_query = f"INSERT INTO main.names VALUES (NULL, '{name}');"
    conn = get_db_connection()
    c = get_cursor(conn)
    c.execute(sql_add_name_query)
    conn.commit()
    return name


def is_name_valid(name: str) -> None:
    sql_name_exists_query = f"SELECT name FROM main.names " \
                            f"WHERE name='{name}';"
    conn = get_db_connection()

    c = get_cursor(conn)
    response = c.execute(sql_name_exists_query).fetchone()

    if response:
        raise NameAlreadyExistsError
    elif len(name) > 20:
        raise InvalidNameLengthError


def get_names() -> List[dict]:
    sql_get_names_list_query = "SELECT * FROM main.names"
    conn = get_db_connection()
    c = get_cursor(conn)
    return c.execute(sql_get_names_list_query).fetchall()
