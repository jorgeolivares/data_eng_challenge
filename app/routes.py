from flask import request, jsonify

from app import app
from app.models import (
    add_name_to_table,
    get_names,
    is_name_valid,
    NameAlreadyExistsError,
    InvalidNameLengthError
)


@app.route('/')
def home():
    return 'Welcome to the Junior Data Engineering test for LiveEO!!!'


@app.route('/names/add', methods=['GET'])
def add_name():
    name = request.args.get('name')
    try:
        is_name_valid(name)
        add_name_to_table(name=name)
        return f"Name: {name} added to db!", 201

    except NameAlreadyExistsError as e:
        return e.message, e.code

    except InvalidNameLengthError as e:
        return e.message, e.code


@app.route('/names/', methods=['GET'])
def get_names_list():
    names_list = get_names()
    return jsonify(names_list)
