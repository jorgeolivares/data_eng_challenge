import logging
import sqlite3


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


def get_db_connection(db_path: str = 'file::memory:?cache=shared'):
    try:
        conn = sqlite3.connect(db_path)
        return conn
    except sqlite3.Error as e:
        logging.error(e)


def get_cursor(conn: sqlite3.connect):
    conn.row_factory = dict_factory
    return conn.cursor()
